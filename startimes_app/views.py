from django.shortcuts import render

# Create your views here.
from rest_framework.generics import GenericAPIView
from rest_framework import generics, status
from .serializers import ValidationSerializer, PaymentSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
import os
import requests, json
import sys
from datetime import datetime
from requests.packages.urllib3.exceptions import InsecureRequestWarning


now = datetime.now()



requests.packages.urllib3.disable_warnings(InsecureRequestWarning)



base_url            = os.getenv('base_url')

headers = {
            
            'Content-Type': 'application/json'
           
}
Username = os.getenv('USERNAME')
Password = os.getenv('PASSWORD')


    
    
    
        
import ast
new_package_code_str = os.getenv('NEW_PACKAGE_CODE')
new_package_code = ast.literal_eval(new_package_code_str)
package_without_prices = os.getenv('PACKAGE_WITHOUT_PRICES')





class ValidationView(GenericAPIView):
    """
    To validate
    """
    # permission_classes = (IsAuthenticated, )
    serializer_class = ValidationSerializer
    service_status_url = f"{str(base_url)}/v1/service-status"
    validation_url = f"{str(base_url)}/v1/subscribers/"
    
    def get(self, request, *args, **kwargs):
    
        data = request.data
        Bouquet = request.data.get("new_package_code")
        print(f"data in validate: {data}")
      
        try:
            serialized_data = self.serializer_class(data=data)
            print(f"serialized data in validate: {serialized_data}")
            if not serialized_data.is_valid(raise_exception=True):
                return Response({"Message": "Please enter correct details", "status": "Failed"}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print(f"error in first try block of validate: {e.args[0]}")
            return Response({"message": "The service code may not be blank. Please enter a valid service code", "status": "Failed"}, status=status.HTTP_400_BAD_REQUEST)
        
        if Bouquet and Bouquet not in new_package_code:
            return Response({"Response": {"message": f"Please enter the correct Bouquet {package_without_prices}", "status": "Failed"}})
        
        payload = {
            "service_code": data["service_code"],
            "Bouquet": data["new_package_code"],
            "amount" : data['amount']
        }
        print(f"Payload in validate: {payload}")
        try:
            key = os.path.abspath("uba-mtls.key")
            pem_file = os.path.abspath("uba-mtls.pem")
            #this is merchant endopoints that returns the service up time
            response = requests.request("GET", self.service_status_url, verify=False, params=payload, cert=(pem_file, key))
            print(f"response from service_status: {response.text}")
          
            if response.status_code != 200:
                return Response({"Response": {"message": "An error occurred while connecting to host server", "status": "Failed"}}, status=status.HTTP_400_BAD_REQUEST)
            
            url = f"{self.validation_url}{data['service_code']}"
            response = requests.get(url, auth=(Username, Password), verify=False, cert=(pem_file, key))
            print("Checking for active bouquet")
            res = response.json()
            print(f"res in validate: {res}")
            if response.status_code != 200:
                print(res)
                print("Service Code wrong")
                error = response.json()['errorCode']
                if error == 'subscriber_not_found': 
                    print(error)
                    return Response({"Response":{"message": "The subscriber does not exist","status":"Failed"}} )
            
            if res.get("basic_offer_display_name"):
                active_bouquet = res["basic_offer_display_name"]
                bouquet_code = request.data.get("new_package_code")
                if bouquet_code != active_bouquet:
                    if bouquet_code not in new_package_code:
                        return Response({
                            "Response": {
                                "message": "Please select a valid bouquet.",
                                "status": "Failed"
                            }
                        }, status=status.HTTP_400_BAD_REQUEST)

                    valid_amounts = new_package_code[bouquet_code]
                    if isinstance(valid_amounts, list):
                        if data['amount'] not in valid_amounts:
                            valid_amounts_str = ', '.join(map(str, valid_amounts))
                            return Response({
                                "Response": {
                                    "message": f"Please enter a valid amount for {bouquet_code}. Valid amounts are: {valid_amounts_str}",
                                    "status": "Failed"
                                }
                            }, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        if data['amount'] != valid_amounts:
                            return Response({
                                "Response": {
                                    "message": f"Please enter a valid amount for {bouquet_code}. Valid amount is: {valid_amounts}",
                                    "status": "Failed"
                                }
                            }, status=status.HTTP_400_BAD_REQUEST)



                    
                    url = f"{self.validation_url}{data['service_code']}"
                    response = requests.get(url, auth=(Username, Password), verify=False, cert=(pem_file, key))
                    print("Validation successfully done")
                    res = response.json()
                    print(f"res in validate: {res}")
                    # VAS requirement
                    print({
                        "vendor_url": self.service_status_url,
                        "res": str(res) or None,
                        "vas_payload": str(request.data),
                        "vas_url": str(request.path),
                    }, file=sys.stdout)
                    res_message = f"SUBSCRIBER ID: {res.get('subscriber_id')} | SERVICE CODE: {res.get('service_code')} | CUSTOMER NAME: {res.get('customer_name')} | MOBILE: {res.get('mobile')} | AMOUNT: {data['amount']} | ACTIVE BOUQUET: {res.get('basic_offer_display_name')} | SELECTED BOUQUET: {data['new_package_code']}"   
                    return Response({"Response":{"message":res_message,"status":"Success"}}) 

                    
                    
                    
                #logic for handling a match between active bouquet and selected bouquet
                No_bouquet_change_url = f"{self.validation_url}{data['service_code']}/recharge-infos"
                #this is merchant endpoints that returns subscriber's active bouquet
                recharge_infos = requests.get(No_bouquet_change_url, auth=(Username, Password), verify=False, params=payload, cert=(pem_file, key))
                print(f"response in validate: {response.text}")
                if response.status_code != 200:
                    return Response({"Response": {"message": "An error occured whle connecting to host server", "status": "Failed"}}, status=status.HTTP_400_BAD_REQUEST)
                print(recharge_infos)
                final_list = []
                #this is an iteration to get the values of the keys in the bouquet dictionary
                for i in recharge_infos.json():
                    print(i)
                    final_list.append(i.get('amount'))
                if final_list and data['amount'] not in final_list:
                    return Response({"Response": f"Plese enter the correct amount: {final_list}", "status": "Failed"}, status=status.HTTP_400_BAD_REQUEST)
                url = f"{self.validation_url}{data['service_code']}"
                response = requests.get(url,auth=(Username, Password),verify=False,cert=(pem_file, key))
                print("Validation successfully done")
                res = response.json()
                print(f"res in validate: {res}")            
        except Exception as e:
            print(f"error in second try block of validate: {e.args[0]}")
            return Response({"Response": {"message": "An error occurred while connecting to host server", "status": "Failed"}}, status=status.HTTP_400_BAD_REQUEST)
        # VAS requirement
        print({
            "vendor_url": self.service_status_url,
            "res": str(res) or None,
            "vas_payload": str(request.data),
            "vas_url": str(request.path),
        }, file=sys.stdout)
        res_message = f"SUBSCRIBER ID: {res.get('subscriber_id')} | SERVICE CODE: {res.get('service_code')} | CUSTOMER NAME: {res.get('customer_name')} | MOBILE: {res.get('mobile')} | AMOUNT: {data['amount']} | ACTIVE BOUQUET: {res.get('basic_offer_display_name')} | SELECTED BOUQUET: {data['new_package_code']}" 
        return Response({"Response":{"message":res_message,"status":"Success"}}) 

class PaymentView(GenericAPIView):
    # permission_classes = (IsAuthenticated, )
    
    serializer_class    = PaymentSerializer
    url                 = f"{str(base_url)}/v1/recharging"
    validation_url      = f"{str(base_url)}/v1/subscribers/"

    def post(self, request, *args, **kwargs):
        key = os.path.abspath("uba-mtls.key")
        pem_file = os.path.abspath("uba-mtls.pem")
        print("Inside post in payment")
        print(f"request.data: {request.data}")
        data = request.data
        print (f"data in payment: {data}")
        serialized_data = self.serializer_class(data = data)
        print (f"serialized data in payment: {serialized_data}")
        if not serialized_data.is_valid(raise_exception=True):
            return Response({"Response": {"message" : "Please enter the correct details to proceed with payment", "status":"Failed"}}, status=status.HTTP_400_BAD_REQUEST)
        print("Information is verified.")
        payment_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        payload = {
                    
                    "serial_no"             : request.data.get("tranref"), 
                    "transaction_time"      : payment_time,
                    "service_code"          : request.data.get("service_code"),
                    "amount"                : request.data.get("amount"),
                    "new_package_code"      : request.data.get('new_package_code'),
                    "payment_description"   : request.data.get("payment_description"),
                    "mobile"                : request.data.get("mobile")
        }
        print (f"Payload in payment: {payload}")
        
        
        try:
            key = os.path.abspath("uba-mtls.key")
            pem_file = os.path.abspath("uba-mtls.pem")
            response = requests.request("POST", self.url, auth=(Username, Password), verify=False, data=json.dumps(payload),headers=headers, cert=(pem_file, key))
            print (f"response in payment: {response.text}")
            res = response.json()
            print (f"res in payment: {res}")
            try:
                if response.status_code != 200:
                    print(res)
                    print("Payment failed")
                    error = response.json()['errorCode']
                    if error == 'duplicate_serial_no': 
                        print(error)
                        return Response({"Response":{"message": "Transaction serial number is a duplicate","status":"Failed"}} )
                    if error == 'invalid_mobile': 
                        print(error)
                        return Response({"Response":{"message": "The format of telephone is incorrect","status":"Failed"}} )
                
            except Exception as e:
                print("Status not available. Check host connection")
                return Response({"Response":{"message": "Status not available. Check host connection","status":"Failed"}} )
        except Exception as e:
            print("Unable to make payment for now")
            return  Response({"Response":{"message":"We are unable to make payment for now ", "status":"Failed"}})
        print({
            "vendor_url": self.url,
            "res": str(res) or None,
            "vas_payload": str(request.data),
            "vas_url": str(request.path),
        }, file=sys.stdout)
        
        print("Payment completed successfully")
        return Response({"Response":{"message": "Payment completed successfully", "status":"Success"}})












